import turtle
import time

def narysujProstokat(a,b):
    t.forward(a)
    t.right(90)
    t.forward(b)
    t.right(90)
    t.forward(a)
    t.right(90)
    t.forward(b)

def narysujKwadrat(a):
    t.forward(a)
    t.right(90)
    t.forward(a)
    t.right(90)
    t.forward(a)
    t.right(90)
    t.forward(a)

def narysujTrojkat(a):
    t.forward(a)
    t.right(60)
    t.forward(a)
    t.right(150)
    t.forward(2*a)

def narysujKolo(a):
    t.circle(a)

wn = turtle.Screen()
wn.bgcolor("white")
t = turtle.Turtle()
t.pensize(5)

figura = raw_input("Jaka figure narysowac?: ")

def podajKolor():
    kolor = raw_input("Podaj kolor: ")
    return kolor

if(figura == "kwadrat"):
    t.color(podajKolor())
    narysujKwadrat(100)
elif(figura == "trojkat"):
    t.color(podajKolor())
    narysujTrojkat(100)
elif (figura == "kolo"):
    t.color(podajKolor())
    narysujKolo(100)
elif(figura == "prostokat"):
    t.color(podajKolor())
    narysujProstokat(100,150)
else:
    print "Brak podanej figury"

time.sleep(5)


